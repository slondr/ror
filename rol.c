#define ror(x) (unsigned char)((unsigned char)  ((x >> 1) | (x << 7)))
#define rol(x) (unsigned char)((unsigned char)  ((x << 1) | (x >> 7)))

int main(int argc, char ** argv) {
  unsigned char byte = (unsigned char) argv[1][0];
  return ror(byte);
}
